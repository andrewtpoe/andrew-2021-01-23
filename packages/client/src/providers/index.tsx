import DocumentsContextProvider from './documents';

type TAppProvidersProps = {
  children: React.ReactChild | React.ReactChildren;
};

function AppProviders({ children }: TAppProvidersProps) {
  return <DocumentsContextProvider>{children}</DocumentsContextProvider>;
}

export default AppProviders;
