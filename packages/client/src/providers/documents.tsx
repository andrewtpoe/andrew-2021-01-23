import * as React from 'react';
import { makeAPIURL } from '../utils/api';

type TDocumentContextProviderProps = {
  children: React.ReactChild | React.ReactChildren;
};

type TDocumentData = {
  fileName: string;
  originalName: string;
  size: number;
  url: string;
};

type TDocuments = TDocumentData[];

export type TDocumentContext = {
  deleteDocument: (fileName: string) => void;
  documents?: TDocuments;
  error: string | null;
  loading: boolean;
  query: string;
  refresh: () => void;
  search: (query: string) => void;
};

const defaultContext = {
  deleteDocument: () => {},
  documents: undefined,
  error: null,
  loading: false,
  query: '',
  refresh: () => {},
  search: () => {},
};

export const DocumentContext = React.createContext<TDocumentContext>(
  defaultContext,
);

export default function DocumentContextProvider({
  children,
}: TDocumentContextProviderProps) {
  const firstRender = React.useRef(true);
  const [state, setState] = React.useState<
    Pick<TDocumentContext, 'documents' | 'error' | 'loading' | 'query'>
  >({
    documents: undefined,
    error: null,
    loading: false,
    query: '',
  });

  const loadDocuments = React.useCallback(
    async (query: string = '') => {
      setState((currentState) => ({
        ...currentState,
        documents: undefined,
        error: null,
        loading: true,
        query,
      }));

      const response = await fetch(makeAPIURL('/images'));
      const responseBody = await response.json();

      if (response.ok) {
        const {
          data: { documents },
        } = responseBody;
        setState((currentState) => ({
          ...currentState,
          documents,
          loading: false,
        }));
        return;
      }

      setState((currentState) => ({
        ...currentState,
        error: responseBody.message,
        loading: false,
      }));
    },
    [setState],
  );

  const deleteDocument = React.useCallback(
    async (fileName: string) => {
      const response = await fetch(
        makeAPIURL(`/images/${encodeURI(fileName)}`),
        {
          method: 'DELETE',
        },
      );

      if (response.ok) {
        loadDocuments();
        return;
      }

      setState((currentState) => ({
        ...currentState,
        error: `Error while deleting image at fileName: ${fileName}`,
      }));
    },
    [loadDocuments, setState],
  );

  React.useEffect(() => {
    if (firstRender.current) loadDocuments();
    firstRender.current = false;
  }, []);

  const value = React.useMemo(
    () => ({
      documents: state.documents,
      error: state.error,
      loading: state.loading,
      query: state.query,
      deleteDocument: (fileName: string) => {
        deleteDocument(fileName);
      },
      refresh: () => {
        loadDocuments(state.query);
      },
      search: () => {
        console.log('[DocumentContext] search: TODO');
      },
    }),
    [deleteDocument, loadDocuments, state],
  );

  return (
    <DocumentContext.Provider value={value}>
      {children}
    </DocumentContext.Provider>
  );
}
