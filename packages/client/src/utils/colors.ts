export default {
  danger: '#dc2626',
  dangerLight: '#f87171',
  info: '#2563eb',
  primary: '#2563eb',
  secondary: '#60a5fa',
  success: '#059669',
  text: '#1f2937',
  textSecondary: '#6b7280',
  warning: '#fbbf24',
  white: '#ffffff',
} as const;
