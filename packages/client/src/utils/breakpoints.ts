export enum BREAKPOINTS {
  MD = 640,
  LG = 960,
}

type MediaQuery = string;

export const breakpoint = (dimension: BREAKPOINTS): MediaQuery =>
  `@media (min-width: ${dimension}px)`;
