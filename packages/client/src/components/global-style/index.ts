import { createGlobalStyle } from 'styled-components';

import colors from '../../utils/colors';

export default createGlobalStyle`
  html {
    box-sizing: border-box;
  }
  body {
    color: ${colors.text};
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
  }
  *, *:before, *:after {
    box-sizing: inherit;
  }
`;
