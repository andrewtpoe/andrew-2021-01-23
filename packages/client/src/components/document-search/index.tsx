import { BREAKPOINTS, breakpoint } from '../../utils/breakpoints';

function DocumentSearch() {
  return (
    <input
      css={`
        margin-bottom: 16px;
        padding: 8px;
        width: 100%;

        ${breakpoint(BREAKPOINTS.MD)} {
          margin: 0;
        }
      `}
      disabled
      type="search"
      id="search-documents"
      name="search-documents"
      aria-label="Search for documents that have been uploaded"
      placeholder="Search documents..."
    />
  );
}

export default DocumentSearch;
