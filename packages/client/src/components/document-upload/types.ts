export type TDocumentUploadProps = {
  onUpload?: () => void;
};
