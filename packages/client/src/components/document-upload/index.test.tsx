import '@testing-library/jest-dom';

import * as React from 'react';
import {
  RenderOptions,
  render as rtlRender,
  RenderResult,
  waitFor,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import AppProviders from '../../providers';

import DocumentUpload from '.';

beforeEach(() => {
  // On rendering the component providers, an initial request will be sent
  // to query for uploaded documents.
  global.fetch = jest.fn().mockReturnValueOnce(
    (Promise.resolve({
      json: () => Promise.resolve({ data: { documents: [] } }),
      ok: true,
    }) as unknown) as typeof fetch,
  );
});

const render = (
  ui: React.ReactElement,
  options: RenderOptions = {},
): RenderResult =>
  rtlRender(ui, {
    wrapper: ({ children }: { children: React.ReactChildren }) => (
      <AppProviders>{children}</AppProviders>
    ),
    ...options,
  } as RenderOptions);

describe('DocumentUpload', () => {
  describe('uploading a file', () => {
    describe('that is an invalid type', () => {
      it('renders an error and does NOT submit the file', async () => {
        const { queryByText } = render(<DocumentUpload />);

        const fileInput = document.querySelector('input[type="file"]');

        const file = new File(['random text'], 'random-text.txt', {
          type: 'text/plain',
        });

        userEvent.upload(fileInput as Element, [file]);

        expect(
          await queryByText('File must be a JPG or PNG.'),
        ).toBeInTheDocument();

        // This makes the test complete after all of the fetch promises and related state updates,
        // removing some ugly warnings in the test output.
        await waitFor(() => queryByText('UPLOAD'));
      });
    });

    describe('that is valid', () => {
      it('submits the file over the api and refreshes the data', async () => {
        const fileName = 'fake.png';

        const fetchMock = jest
          .fn()
          .mockReturnValueOnce(
            (Promise.resolve({
              json: () => Promise.resolve({ data: { documents: [] } }),
              ok: true,
            }) as unknown) as typeof fetch,
          )
          .mockReturnValueOnce(
            (Promise.resolve({
              json: () =>
                Promise.resolve({
                  data: {
                    document: {
                      fileName,
                      originalName: fileName,
                      size: 28,
                      url: 'teeny-test_-_28_-_fake.png',
                    },
                  },
                }),
              ok: true,
            }) as unknown) as typeof fetch,
          )
          .mockReturnValueOnce(
            (Promise.resolve({
              json: () =>
                Promise.resolve({
                  data: {
                    documents: [
                      {
                        fileName,
                        originalName: fileName,
                        size: 28,
                        url: 'teeny-test_-_28_-_fake.png',
                      },
                    ],
                  },
                }),
              ok: true,
            }) as unknown) as typeof fetch,
          );
        global.fetch = fetchMock;

        const { queryByText } = render(<DocumentUpload />);

        const fileInput = document.querySelector('input[type="file"]');
        const file = new File(['random text'], fileName, {
          type: 'image/png',
        });
        userEvent.upload(fileInput as Element, [file]);

        await waitFor(() =>
          expect(
            queryByText('LOADING', { selector: 'button' }),
          ).toBeInTheDocument(),
        );
        await waitFor(() => expect(queryByText('UPLOAD')).toBeInTheDocument());
        expect(fetchMock).toHaveBeenCalledTimes(3);
      });

      describe('when the api returns an error', () => {
        it('renders an upload error', async () => {
          const fileName = 'fake.png';

          const fetchMock = jest
            .fn()
            .mockReturnValueOnce(
              (Promise.resolve({
                json: () => Promise.resolve({ data: { documents: [] } }),
                ok: true,
              }) as unknown) as typeof fetch,
            )
            .mockReturnValueOnce(
              (Promise.resolve({
                json: () =>
                  Promise.resolve({
                    message: 'ERROR',
                  }),
                ok: false,
              }) as unknown) as typeof fetch,
            );
          global.fetch = fetchMock;

          const { queryByText } = render(<DocumentUpload />);

          const fileInput = document.querySelector('input[type="file"]');
          const file = new File(['random text'], fileName, {
            type: 'image/png',
          });
          userEvent.upload(fileInput as Element, [file]);

          await waitFor(() =>
            expect(
              queryByText('LOADING', { selector: 'button' }),
            ).toBeInTheDocument(),
          );
          await waitFor(() =>
            expect(queryByText('UPLOAD')).toBeInTheDocument(),
          );

          expect(fetchMock).toHaveBeenCalledTimes(2);

          await waitFor(() =>
            expect(
              queryByText('File could not be uploaded.'),
            ).toBeInTheDocument(),
          );
        });
      });
    });
  });
});
