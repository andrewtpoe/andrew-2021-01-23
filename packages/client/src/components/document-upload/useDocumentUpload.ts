import * as React from 'react';

import { makeAPIURL } from '../../utils/api';

import { TDocumentUploadProps } from './types';

const MAX_FILE_SIZE = 10240000; // 10MB
const VALID_FILE_TYPES = ['jpg', 'png'];
const FILE_TYPE_REGEX = new RegExp(`.*.(${VALID_FILE_TYPES.join('|')})`, 'gi');

const isFileSizeValid = (file: File) => file.size < MAX_FILE_SIZE;
const isFileTypeValid = (value: string) => value.match(FILE_TYPE_REGEX);

enum DOCUMENT_ERRORS {
  INVALID_TYPE = `File must be a JPG or PNG.`,
  TOO_LARGE = `File must be smaller than 10MB.`,
  UPLOAD_ERROR = `File could not be uploaded.`,
}

enum DOCUMENT_UPLOAD_STATUS {
  LOADING,
}

enum DOCUMENT_UPLOAD_ACTION_TYPE {
  SET_DOCUMENT_ERROR,
  SET_UPLOAD_STATUS,
}

type TDocumentUploadState = {
  documentError: DOCUMENT_ERRORS | null;
  documentUploadStatus: DOCUMENT_UPLOAD_STATUS | null;
};

type TSetDocumentErrorAction = {
  type: DOCUMENT_UPLOAD_ACTION_TYPE.SET_DOCUMENT_ERROR;
  value: DOCUMENT_ERRORS | null;
};
type TSetDocumentUploadStatusAction = {
  type: DOCUMENT_UPLOAD_ACTION_TYPE.SET_UPLOAD_STATUS;
  value: DOCUMENT_UPLOAD_STATUS | null;
};

type TDocumentUploadActions =
  | TSetDocumentErrorAction
  | TSetDocumentUploadStatusAction;

function documentUploadReducer(
  state: TDocumentUploadState,
  action: TDocumentUploadActions,
): TDocumentUploadState {
  switch (action.type) {
    case DOCUMENT_UPLOAD_ACTION_TYPE.SET_DOCUMENT_ERROR:
      return { ...state, documentError: action.value };
    case DOCUMENT_UPLOAD_ACTION_TYPE.SET_UPLOAD_STATUS:
      return { ...state, documentUploadStatus: action.value };
    default:
      return state;
  }
}

const initialState: TDocumentUploadState = {
  documentError: null,
  documentUploadStatus: null,
};

export default function useDocumentUpload({ onUpload }: TDocumentUploadProps) {
  const uploadInputRef = React.useRef<HTMLInputElement>(null);

  const [state, dispatch] = React.useReducer(
    documentUploadReducer,
    initialState,
  );

  // Handles the actual document upload over the API. Gracefully handles errors.
  const handleDocumentSubmit = React.useCallback(
    async (file: File) => {
      dispatch({
        type: DOCUMENT_UPLOAD_ACTION_TYPE.SET_UPLOAD_STATUS,
        value: DOCUMENT_UPLOAD_STATUS.LOADING,
      });

      const formData = new FormData();
      formData.append('document', file);

      const response = await fetch(makeAPIURL('/images'), {
        method: 'POST',
        body: formData,
      });

      dispatch({
        type: DOCUMENT_UPLOAD_ACTION_TYPE.SET_UPLOAD_STATUS,
        value: null,
      });

      if (response.ok) {
        if (onUpload) onUpload();
      } else {
        dispatch({
          type: DOCUMENT_UPLOAD_ACTION_TYPE.SET_DOCUMENT_ERROR,
          value: DOCUMENT_ERRORS.UPLOAD_ERROR,
        });
      }
    },
    [dispatch, onUpload],
  );

  const onChangeUploadInput = React.useCallback(
    async ({
      currentTarget: { files },
    }: React.ChangeEvent<HTMLInputElement>) => {
      const file = (files ?? [])[0];

      if (!isFileTypeValid(file.name)) {
        dispatch({
          type: DOCUMENT_UPLOAD_ACTION_TYPE.SET_DOCUMENT_ERROR,
          value: DOCUMENT_ERRORS.INVALID_TYPE,
        });
        return;
      }

      if (!isFileSizeValid(file)) {
        dispatch({
          type: DOCUMENT_UPLOAD_ACTION_TYPE.SET_DOCUMENT_ERROR,
          value: DOCUMENT_ERRORS.TOO_LARGE,
        });
        return;
      }

      handleDocumentSubmit(file);
    },
    [dispatch, handleDocumentSubmit],
  );

  // For an improved UI, we are using a button to kick off the upload.
  // All this button does though is proxy the click to the file input.
  const onClickUploadButton = React.useCallback(() => {
    dispatch({
      type: DOCUMENT_UPLOAD_ACTION_TYPE.SET_DOCUMENT_ERROR,
      value: null,
    });
    uploadInputRef.current?.click();
  }, [dispatch, uploadInputRef]);

  return React.useMemo(
    () => ({
      error: state.documentError,
      loading: state.documentUploadStatus === DOCUMENT_UPLOAD_STATUS.LOADING,
      onChangeUploadInput,
      onClickUploadButton,
      uploadInputRef,
    }),
    [state, onChangeUploadInput, onClickUploadButton],
  );
}
