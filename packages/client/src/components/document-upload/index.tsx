import * as React from 'react';

import { DocumentContext } from '../../providers/documents';

import { BREAKPOINTS, breakpoint } from '../../utils/breakpoints';
import colors from '../../utils/colors';

import { TDocumentUploadProps } from './types';
import useDocumentUpload from './useDocumentUpload';

const DOCUMENT_UPLOAD_INPUT_ID = 'document-upload';

function DocumentUpload({ onUpload }: TDocumentUploadProps) {
  const { documents = [], refresh: refreshDocuments } = React.useContext(
    DocumentContext,
  );
  const {
    error,
    loading,
    onChangeUploadInput,
    onClickUploadButton,
    uploadInputRef,
  } = useDocumentUpload({
    onUpload: () => {
      if (onUpload) onUpload();
      refreshDocuments();
    },
  });

  return (
    <>
      <button
        css={`
          background-color: ${colors.primary};
          color: ${colors.white};
          width: 100%;
          margin-bottom: 16px;
          padding: 8px 16px;
          border-radius: 2px;
          border: none;

          :active,
          :hover,
          :focus {
            background-color: ${colors.secondary};
          }

          :disabled {
            background-color: ${colors.textSecondary};
          }

          ${breakpoint(BREAKPOINTS.MD)} {
            margin: 0;
            max-width: 200px;
          }
        `}
        disabled={loading}
        onClick={onClickUploadButton}
        type="button"
      >
        {loading ? 'LOADING' : 'UPLOAD'}
      </button>
      {error && (
        <span
          css={`
            color: ${colors.danger};
            display: block;
            margin-bottom: 16px;

            ${breakpoint(BREAKPOINTS.MD)} {
              position: absolute;
              right: 0;
              top: calc(100% + 8px);
              white-space: nowrap;
            }
          `}
        >
          {error}
        </span>
      )}
      <input
        key={`document-upload-${documents.length}`}
        accept="image/jpg, image/png"
        disabled={loading}
        hidden
        id={DOCUMENT_UPLOAD_INPUT_ID}
        name="document-upload"
        onChange={onChangeUploadInput}
        ref={uploadInputRef}
        type="file"
      />
    </>
  );
}

export default DocumentUpload;
