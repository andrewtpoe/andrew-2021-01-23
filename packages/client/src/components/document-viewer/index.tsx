import * as React from 'react';

import { DocumentContext } from '../../providers/documents';

import { BREAKPOINTS, breakpoint } from '../../utils/breakpoints';

import ErrorPanel from './error-panel';
import LoadingPanel from './loading-panel';
import EmptyPanel from './empty-panel';
import ResultsPanel from './results-panel';

function DocumentViewer() {
  const { deleteDocument, documents = [], error, loading } = React.useContext(
    DocumentContext,
  );

  const MainPanel = React.useCallback(() => {
    if (error) return <ErrorPanel />;
    if (loading) return <LoadingPanel />;
    if (documents.length === 0) return <EmptyPanel />;

    return (
      <ResultsPanel deleteDocument={deleteDocument} documents={documents} />
    );
  }, [deleteDocument, documents, error, loading]);

  const totalKBSize = React.useMemo(() => {
    const byteSize = documents.reduce((acc: number, { size }) => acc + size, 0);
    return Math.round(byteSize / 1024);
  }, [documents]);

  return (
    <section>
      <div
        css={`
          ${breakpoint(BREAKPOINTS.MD)} {
            align-items: flex-end;
            display: flex;
            justify-content: space-between;
            margin: 16px 0;
          }
        `}
      >
        <h2
          css={`
            font-size: 32px;
            margin: 8px 0;

            ${breakpoint(BREAKPOINTS.MD)} {
              font-size: 40px;
              margin: 0;
            }
          `}
        >
          {`${documents.length} Document${documents.length !== 1 ? 's' : ''}`}
        </h2>
        <p
          css={`
            margin: 0;
            margin-bottom: 8px;

            ${breakpoint(BREAKPOINTS.MD)} {
              /* Promotes straight bottom alignment with the h2 */
              margin-bottom: 4px;
            }
          `}
        >
          Total size: {totalKBSize} kb
        </p>
      </div>
      <MainPanel />
    </section>
  );
}

export default DocumentViewer;
