import '@testing-library/jest-dom';

import * as React from 'react';
import {
  RenderOptions,
  render as rtlRender,
  RenderResult,
  waitFor,
  fireEvent,
  getByLabelText,
} from '@testing-library/react';

import AppProviders from '../../providers';

import DocumentViewer from '.';
import { makeAPIURL } from '../../utils/api';

beforeEach(() => {
  // On rendering the component providers, an initial request will be sent
  // to query for uploaded documents.
  global.fetch = jest.fn().mockReturnValueOnce(
    (Promise.resolve({
      json: () => Promise.resolve({ data: { documents: [] } }),
      ok: true,
    }) as unknown) as typeof fetch,
  );
});

const render = (
  ui: React.ReactElement,
  options: RenderOptions = {},
): RenderResult =>
  rtlRender(ui, {
    wrapper: ({ children }: { children: React.ReactChildren }) => (
      <AppProviders>{children}</AppProviders>
    ),
    ...options,
  } as RenderOptions);

describe('DocumentViewer', () => {
  describe('while loading documents', () => {
    it('renders a loading message and zeroed out title info', async () => {
      const { queryByText } = render(<DocumentViewer />);

      await waitFor(() => {
        expect(queryByText('Loading...')).toBeInTheDocument();
        expect(queryByText('0 Documents')).toBeInTheDocument();
        expect(queryByText('Total size: 0 kb')).toBeInTheDocument();
      });
    });
  });

  describe('with an error when loading documents', () => {
    it('renders an error state message and zeroed out title info', async () => {
      const fetchMock = jest.fn().mockReturnValueOnce(
        (Promise.resolve({
          json: () =>
            Promise.resolve({
              message: 'ERROR',
            }),
          ok: false,
        }) as unknown) as typeof fetch,
      );
      global.fetch = fetchMock;

      const { queryByText } = render(<DocumentViewer />);

      await waitFor(() => {
        expect(
          queryByText('There was an error loading the documents.'),
        ).toBeInTheDocument();
        expect(queryByText('0 Documents')).toBeInTheDocument();
        expect(queryByText('Total size: 0 kb')).toBeInTheDocument();
      });
    });
  });

  describe('with no documents', () => {
    it('renders an empty state message and zeroed out title info', async () => {
      const { queryByText } = render(<DocumentViewer />);

      await waitFor(() => {
        expect(
          queryByText('There are no uploaded documents yet.'),
        ).toBeInTheDocument();
        expect(queryByText('0 Documents')).toBeInTheDocument();
        expect(queryByText('Total size: 0 kb')).toBeInTheDocument();
      });
    });
  });

  describe('with documents', () => {
    const file0 = {
      fileName: 'date_-_1200000_-_test-pic-0.png',
      originalName: 'test-pic-0.png',
      size: 1200000,
      url: '/#test-pic-0',
    };
    const file1 = {
      fileName: 'date_-_12000_-_test-pic-1.png',
      originalName: 'test-pic-1.png',
      size: 12000,
      url: '/#test-pic-1',
    };

    const makeFetchMock = () =>
      jest.fn().mockReturnValueOnce(
        (Promise.resolve({
          json: () =>
            Promise.resolve({
              data: { documents: [file0, file1] },
            }),
          ok: true,
        }) as unknown) as typeof fetch,
      );

    beforeEach(() => {
      global.fetch = makeFetchMock();
    });

    it('renders the images and accurate sums in the title info', async () => {
      const { queryByAltText, queryByText } = render(<DocumentViewer />);

      await waitFor(() => {
        expect(queryByText('2 Documents')).toBeInTheDocument();
        expect(queryByText('Total size: 1184 kb')).toBeInTheDocument();
      });

      await waitFor(() => {
        expect(queryByAltText(file0.originalName)).toBeInTheDocument();
        expect(queryByAltText(file1.originalName)).toBeInTheDocument();
      });
    });

    describe('deleting a document', () => {
      it('submits the delete request over the api and reloads the documents', async () => {
        const fetchMock = makeFetchMock()
          .mockReturnValueOnce(
            (Promise.resolve({
              json: () => Promise.resolve({}),
              ok: true,
            }) as unknown) as typeof fetch,
          )
          .mockReturnValueOnce(
            (Promise.resolve({
              json: () =>
                Promise.resolve({
                  data: { documents: [file0] },
                }),
              ok: true,
            }) as unknown) as typeof fetch,
          );
        global.fetch = fetchMock;

        const { queryByAltText, getByLabelText, queryByText } = render(
          <DocumentViewer />,
        );

        await waitFor(() => {
          expect(queryByAltText(file0.originalName)).toBeInTheDocument();
          expect(queryByAltText(file1.originalName)).toBeInTheDocument();
        });

        await waitFor(() => {
          expect(queryByText('2 Documents')).toBeInTheDocument();
          expect(queryByText('Total size: 1184 kb')).toBeInTheDocument();
        });

        fireEvent.click(getByLabelText(`Delete ${file1.originalName}`));

        await waitFor(() => {
          expect(queryByText('Loading...')).toBeInTheDocument();
        });

        await waitFor(() => {
          expect(queryByAltText(file0.originalName)).toBeInTheDocument();
          expect(queryByAltText(file1.originalName)).not.toBeInTheDocument();
        });

        await waitFor(() => {
          expect(queryByText('1 Document')).toBeInTheDocument();
          expect(queryByText('Total size: 1172 kb')).toBeInTheDocument();
        });

        expect(fetchMock).toHaveBeenCalledTimes(3);
        expect(fetchMock).toHaveBeenNthCalledWith(
          2,
          makeAPIURL(`/images/${encodeURI(file1.fileName)}`),
          { method: 'DELETE' },
        );
      });
    });
  });
});
