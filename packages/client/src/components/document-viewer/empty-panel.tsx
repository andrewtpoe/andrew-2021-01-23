export default function EmptyPanel() {
  return (
    <div
      css={`
        display: flex;
        justify-content: center;
        margin: 80px 0;
      `}
    >
      <p>There are no uploaded documents yet.</p>
    </div>
  );
}
