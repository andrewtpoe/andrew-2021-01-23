export default function LoadingPanel() {
  return (
    <div
      css={`
        display: flex;
        justify-content: center;
        margin: 80px 0;
      `}
    >
      <p>Loading...</p>
    </div>
  );
}
