import colors from '../../utils/colors';

export default function ErrorPanel() {
  return (
    <div
      css={`
        display: flex;
        justify-content: center;
        margin: 80px 0;
      `}
    >
      <p
        css={`
          color: ${colors.danger};
        `}
      >
        There was an error loading the documents.
      </p>
    </div>
  );
}
