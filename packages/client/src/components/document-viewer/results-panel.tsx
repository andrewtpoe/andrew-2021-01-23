import Image from 'next/image';
import * as React from 'react';

import { TDocumentContext } from '../../providers/documents';

import { BREAKPOINTS, breakpoint } from '../../utils/breakpoints';
import colors from '../../utils/colors';

type TResultsPanelProps = Pick<
  TDocumentContext,
  'documents' | 'deleteDocument'
>;

export default function ResultsPanel({
  deleteDocument,
  documents,
}: TResultsPanelProps) {
  return (
    <div
      css={`
        column-gap: 16px;
        display: grid;
        grid-template-columns: 1fr;
        row-gap: 16px;

        ${breakpoint(BREAKPOINTS.MD)} {
          grid-template-columns: 1fr 1fr;
        }

        ${breakpoint(BREAKPOINTS.LG)} {
          grid-template-columns: 1fr 1fr 1fr;
        }
      `}
    >
      {documents?.map(({ fileName, originalName, size, url }) => (
        <div
          key={url}
          css={`
            border: 1px solid ${colors.text};
            border-radius: 8px;
            width: 100%;
            min-height: 80px;
            overflow: hidden;
          `}
        >
          <Image
            css={`
              object-fit: contain;
              height: 100%;
              width: 100%;
            `}
            alt={originalName}
            width={500}
            height={500}
            src={url}
          />
          <div>
            <div
              css={`
                padding: 8px 0;
              `}
            >
              <p
                css={`
                  margin: 8px 16px;
                `}
              >
                {originalName}
              </p>
              <div
                css={`
                  display: flex;
                  align-items: center;
                  justify-content: space-between;
                `}
              >
                <p
                  css={`
                    margin: 8px 16px;
                  `}
                >
                  {Math.round(size / 1024)} kb
                </p>
                <div
                  css={`
                    margin: 0 16px;
                  `}
                >
                  <button
                    aria-label={`Delete ${originalName}`}
                    css={`
                      background-color: ${colors.white};
                      border-radius: 2px;
                      border: 1px solid ${colors.danger};
                      color: ${colors.danger};
                      padding: 4px 8px;

                      :active,
                      :hover,
                      :focus {
                        background-color: ${colors.danger};
                        color: ${colors.white};
                      }
                    `}
                    type="button"
                    onClick={() => deleteDocument(fileName)}
                  >
                    DELETE
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
}
