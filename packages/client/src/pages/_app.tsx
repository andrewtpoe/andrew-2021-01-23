import 'normalize.css';

import GlobalStyle from '../components/global-style';
import AppProviders from '../providers';

type TAppProps = {
  Component: React.ComponentType;
  pageProps: object;
};

function App({ Component, pageProps }: TAppProps) {
  return (
    <>
      <GlobalStyle />
      <AppProviders>
        <Component {...pageProps} />
      </AppProviders>
    </>
  );
}

export default App;
