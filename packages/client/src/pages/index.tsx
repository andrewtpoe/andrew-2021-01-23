import Head from 'next/head';

import DocumentSearch from '../components/document-search';
import DocumentUpload from '../components/document-upload';
import DocumentViewer from '../components/document-viewer';

import { BREAKPOINTS, breakpoint } from '../utils/breakpoints';

export default function Home() {
  return (
    <>
      <Head>
        <title>Uploadr</title>
      </Head>
      <div
        css={`
          margin: auto;
          max-width: ${BREAKPOINTS.LG}px;
          padding: 16px;
        `}
      >
        <header
          css={`
            ${breakpoint(BREAKPOINTS.MD)} {
              display: flex;
              justify-content: space-between;
              margin: 40px 0;
            }
          `}
        >
          <div
            css={`
              ${breakpoint(BREAKPOINTS.MD)} {
                align-items: center;
                display: flex;
                flex: 1;
                justify-content: flex-end;
                order: 1;
                padding-left: 16px;
                position: relative;
              }
            `}
          >
            <DocumentUpload />
          </div>
          <div
            css={`
              ${breakpoint(BREAKPOINTS.MD)} {
                align-items: center;
                display: flex;
                flex: 1;
                order: 0;
              }
            `}
          >
            <DocumentSearch />
          </div>
        </header>
        <main>
          <DocumentViewer />
        </main>
      </div>
    </>
  );
}
