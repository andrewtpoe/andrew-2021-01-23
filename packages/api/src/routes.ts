import express from 'express';

import { deleteImage, getImages, postImage } from './controllers/images';
import uploadFile from './middleware/uploadFile';

const router = express.Router();

export function initRoutes() {
  router.get('/images', getImages);
  router.post('/images', uploadFile.single('document'), postImage);
  router.delete('/images/:fileName', deleteImage);

  return router;
}
