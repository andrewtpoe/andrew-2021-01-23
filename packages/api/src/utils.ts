import createDOMPurify from 'dompurify';
import { JSDOM } from 'jsdom';

const VALID_FILE_TYPES = ['jpg', 'png'];
const FILE_TYPE_REGEX = new RegExp(`.*.(${VALID_FILE_TYPES.join('|')})`, 'gi');
export const isFileNameValid = (fileName: string) =>
  Boolean(fileName.match(FILE_TYPE_REGEX));

const window = new JSDOM('').window;
export const DOMPurify = createDOMPurify((window as unknown) as Window);
