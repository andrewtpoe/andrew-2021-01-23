import multer from 'multer';

import { UPLOAD_DELIMITER, UPLOADS_DIR_PATH } from '../constants';
import { DOMPurify, isFileNameValid } from '../utils';

const MAX_FILE_SIZE = 10240000; // 10MB

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, UPLOADS_DIR_PATH);
  },
  filename: (req, file, cb) => {
    // The actual stored file gets a few added values:
    // - A timestamp to ensure uniqueness of the file
    // - The uploaded file size
    const fileName = `${new Date().getTime()}${UPLOAD_DELIMITER}FILE_SIZE${UPLOAD_DELIMITER}${DOMPurify.sanitize(
      file.originalname,
      { ALLOWED_TAGS: [] },
    )}`;

    cb(null, fileName);
  },
});

export default multer({
  storage,
  fileFilter: (req, file, cb) => {
    cb(
      null,
      isFileNameValid(
        DOMPurify.sanitize(file.originalname, { ALLOWED_TAGS: [] }),
      ),
    );
  },
  limits: { fileSize: MAX_FILE_SIZE },
});
