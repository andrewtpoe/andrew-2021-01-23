import { RequestHandler } from 'express';
import fs from 'fs';

import { UPLOADS_DIR_PATH, UPLOAD_DELIMITER } from '../../constants';
import { DOMPurify, isFileNameValid } from '../../utils';

import { renameUploadedImage } from './services';

export const deleteImage: RequestHandler = async (req, res) => {
  try {
    if (!isFileNameValid(req.params.fileName)) throw 'Invalid fileName';
    if (
      req.params.fileName.startsWith('.') ||
      req.params.fileName.startsWith('/')
    )
      throw 'Inavlid fileName';

    fs.unlink(`${UPLOADS_DIR_PATH}/${req.params.fileName}`, (err) => {
      if (err) throw err;

      res.status(204).send();
    });
  } catch (err) {
    res.status(500).send({ message: `Unable to delete image. ${err}` });
  }
};

type TDocumentData = {
  fileName: string;
  originalName: string;
  size: number;
  url: string;
};

type TDocuments = TDocumentData[];

const makeDocumentData = (
  req: { hostname: string },
  fileName: string,
): TDocumentData => {
  const [, size, originalName] = fileName.split(UPLOAD_DELIMITER);

  return {
    fileName,
    originalName,
    size: parseInt(size, 10),
    url: `http://${req.hostname}:${process.env.SERVER_PORT}/static/assets/images/${fileName}`,
  };
};

export const getImages: RequestHandler = async (req, res) => {
  try {
    fs.readdir(UPLOADS_DIR_PATH, (err, files) => {
      if (err) throw err;

      const documents = files
        .map((fileName) => {
          // There may be extra files that shouldn't be sent, like `.gitkeep`.
          if (isFileNameValid(fileName)) return makeDocumentData(req, fileName);
        })
        .filter(Boolean) as TDocuments;

      res.status(200).send({ data: { documents } });
    });
  } catch (err) {
    res.status(500).send({
      message: `Unable to load images. ${err}`,
    });
  }
};

export const postImage: RequestHandler = async (req, res, cb) => {
  try {
    renameUploadedImage(req, res, cb);

    if (!req.file) {
      return res.status(400).send({ message: 'Upload failed. No file.' });
    }

    res
      .status(201)
      .send({ data: { document: makeDocumentData(req, req.file.filename) } });
  } catch (err) {
    res.status(500).send({
      message: `Upload failed for file ${DOMPurify.sanitize(
        req.file.originalname,
        { ALLOWED_TAGS: [] },
      )}. ${err}`,
      DOMPurify,
    });
  }
};
