import { RequestHandler } from 'express';
import fs from 'fs';

import { UPLOAD_DELIMITER } from '../../constants';

export const renameUploadedImage: RequestHandler = (req) => {
  const newFilename = req.file.filename.replace(
    `${UPLOAD_DELIMITER}FILE_SIZE${UPLOAD_DELIMITER}`,
    `${UPLOAD_DELIMITER}${req.file.size}${UPLOAD_DELIMITER}`,
  );
  const pathParts = req.file.path.split('/');
  pathParts[pathParts.length - 1] = newFilename;
  const newPath = pathParts.join('/');

  req.file.filename = newFilename;

  fs.rename(req.file.path, newPath, (err) => {
    if (err) {
      throw err;
    }
  });
};
