# Andrew - 2021-01-23

## Installation

### Prerequisites

- NodeJS 14.15.4
- Yarn

### Setup

- Install the dependencies:
  ```
  $ yarn install
  ```
- Create the .env files populated with the default values:
  ```
  $ cp packages/api/.env.example packages/api/.env
  $ cp packages/client/.env.example packages/client/.env
  ```
  Review the new `.env` files to ensure no changes to the values are required.

### To run in development mode

- Start the development servers:
  ```
  $ yarn start:dev
  ```
- Open your browser to the url logged in the console. The message should look
  like this:
  ```
  client: ready - started server on http://localhost:3000
  ```

### To run in production mode

- Build the packages:
  ```
  $ yarn build
  ```
- Start the production servers:
  ```
  $ yarn start
  ```

### To run the tests

- Start the tests:
  ```
  $ yarn test
  ```

The client also contains a `test:watch` script that makes for easier developing.
It's best to run this script directly from the `/packages/client` directory.

```
$ cd packages/client && yarn test:watch
```

## Security

### Concerns Addressed:

- Many basic node server security concerns are resolved using helmet and cors.
- Some brute force protection built in by disabling uploads until the current
  upload is complete.
- Image uploads are restricted to file types JPG and PNG on both client &
  server.
- Image uploads are restricted to 10mb on both client & server.
- Error handling on API, and UI reports when errors are encountered.
- The DELETE /images route performs a file delete on the server based on the
  user's input. There are some basic validations in place to ensure that only a
  valid file name from the expected directory is being deleted, but this should
  be made more robust.

### Concerns NOT Addressed:

- Image files are persisted on the server using a sanitized name saved on the
  user's computer, and that name is rendered as text within the client. Keeping
  the original name poses a potential security risk, as it opens users to the
  risk of an XSS attack. It may also open the server to risks directly. I've
  sanitized the uploaded filename to help with this, but I've left this as NOT
  addressed because the entire pattern for saving files is poor.

  The optimal way to handle this would be to save the file with a custom name,
  save the extra data (sanitized file name, size, etc...) with a reference id to
  the actual file in a database, and serve the asset based on the id.

- This example API saves uploaded files directly onto your disk. This would be
  extremely bad for anything production grade. A better option would be to save
  these files somewhere like S3.
- No CSRF protection

## Improvements

What could be added to the app / API?

- Image search based on name. This was a requirement outlined in the spec, but I
  ran out of time and wanted to focus on the features I was able to complete.
- Pagination of images
- Enhanced refresh/ delete of images. Would be interesting to maintain the list
  of documents and only flush a changed list when the requests resolve. This
  would eliminate a "flicker" during refresh.
- The above would pair nicely with a more "pretty" loading indicator that is not
  a full panel but maybe an animated spinner in the title area.

## Libraries

### Root Dependencies/ Dev Dependencies

These are used to enhance the development experience. They are not included in
the code when built.

- `lerna`: A toolkit for managing mono-repos. This project is structured as a
  mono-repo with seperate FE & BE packages.
- `concurrently`: A tool to improve running multiple scripts.
- `husky`: Enables simple git hooks. Used with `pretty-quick` to run prettier on
  staged files.
- `prettier`: Code formatting.
- `pretty-quick`: Used with `husky` to run prettier on staged files.

### API Dependencies

- `body-parser`: Used with express to process request body into useable
  attributes.
- `cors`: Enables CORS in express. Required to run the client on a seperate URL.
- `dompurify`: Used to sanitize uploaded file names to prevent XSS attacks.
- `dotenv`: Loads the ENV vars from the `.env` file
- `express`: A lightweight web server
- `helmet`: Security 101 for express.
- `jsdom`: Required to run `dompurify` in node js.
- `multer`: A middleware for handling multipart form data, like file uploads.

### API Dev Dependecies

- `@types/*`: Types packages enable a smooth development experience.
- `nodemon`: Used to watch/ auto re-start the server on file change.
- `ts-node`: Used to run the src directory in development mode.
- `typescript`: Makes JS better.

### Client Dependencies

- `next`: A fantastic toolkit for building React applications.
- `normalize.css`: CSS normalization improves the UI across browsers.
- `react`: A JavaScript library for building user interfaces.
- `react-dom`: Required by `react`.
- `styled-components`: A CSS-in-JS library. Makes styling a breeze!

### Client Dev Dependencies

- `@testing-library/*`: Utilities for testing React applications the way a user
  will use it.
- `@types/*`: Types packages enable a smooth development experience.
- `babel-plugin-styled-components`: Enables the `css` prop used widely within
  the client. Converts components with the `css` prop to styled components under
  the hood. The developer experience of inline styling, but the end result of
  CSS managed fully by styled-components.
- `jest`: A powerful JS testing framework.
- `typescript`: Makes JS better.

## API

The API package is _only_ for use in development and local demo. It is not, nor
is it intended to be, production ready.

All endpoints may either send the expected response outline or an error response
that looks with this data structure:

```
{
  message: <text>
}
```

### GET /images

Description:

- No additional parameters
- Returns a list of data related to documents that have been uploaded.
- A successful response will have the status code 200.
- Response data structure:
  ```
  {
    data: {
      documents: [
        {
          fileName: <name of file when stored>,
          originalName: <name of file when uploaded (sanitized)>,
          size: <number of bytes>,
          url: <url where upload can be accessed.>
        },
        // ...
      ]
    }
  }
  ```

### POST /images

Description:

- Accepts a multipart/form-data with file.
- The file to upload must be sent at fieldname `document`.
- The file must be of type `.jpg` or `.png`.
- The file must be less than 10MB.
- A successful upload will respond with status code 201.
- Response data structure:

  ```
  {
    data: {
      document: {
        fileName: <name of file when stored>,
        originalName: <name of file when uploaded (sanitized)>,
        size: <number of bytes>,
        url: <url where upload can be accessed.>
      }
    }
  }
  ```

### DELETE /images/:fileName

Description:

- Deletes the uploaded file under the name `:fileName`
- A successful delete will have the status code 204 and include no data.
